using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ClickableObject : MonoBehaviour
{
    [SerializeField]
    protected string name;
    
    protected abstract void OnClick();
    
    private void OnMouseDown()
    {
        OnClick();
        UIManager.Instance.SelectedObjectText.text = name;
    }
}
