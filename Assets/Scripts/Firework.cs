using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firework : ClickableObject
{
    [SerializeField]
    private ParticleSystem firerwork;
    
    protected override void OnClick()
    {
        DoFirework();
    }

    private void DoFirework()
    {
        firerwork.Play();
    }
}
